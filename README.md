# Component library based on Fractal

Full documentation: [Fractal User Guide](https://fractal.build/guide/)

## 

*   install [npm](https://www.npmjs.com/package/npm)
*   clone [repository](https://bitbucket.org/techmatesgroup/tmg-fractal-library/src/master/)
*   install dependencies `npm i`
*   start application `npm run start`

## Create component

All components are in the folder `./components`

Component structure:

```
├── components
│   └── componentName
│   │   ├── componentName.config.yml
│   │   ├── componentName.html
```

`componentName.html` - template with `Handlebars` syntax

To include other component:

```handlebars
{{> @otherComponent }}
```

`componentName.config.yml` - configuration file

```yml
# title of component
title: "Sparkly Buttons"

#status (ready|wip|prototype)
status: "prototype"

# context data
context:
    button-text: "Click me!"
    is-sparkly: true
```

## Example of use context data:

[full documentation](https://fractal.build/guide/core-concepts/context-data.html#context-data)

users.config.yml

```yml
context:
  title: Our users
  users:
  - name: Mickey Mouse
    email: mickey@mouse.com
  - name: Daffy Duck
    email: daffy@duck.com
```

users.html

```handlebars
<div>
    <h1>{{ title }}</h1>
    <ul>
        {{#each users}}
        <li>
            <h2>{{ name }}</h2>
            <p>{{ email }}</p>
        </li>
        {{/each}}
    <ul>
</div>
```

## Include css, js

sccs files for component create in assets/scss in the appropriate folder:

```
├── components
│   └── componentName
│   │   ├── componentName.scss
```

based on them will be automatically created .css files in `./public` folder

To include css/js files use `{{ path '/path/from/public/folder/or/url' }}`

Example:

`<link media="all" rel="stylesheet" href="{{ path '//fonts.googleapis.com/css?family=Lato:400,300,700,900|Oswald:700|Rubik:400,300,500,700,700i,300i' }}">`

To include css/js files for all components use common preview 
_preview.html

## Build for deployment

*   `npm run build`
*   deploy files from `./dist`